import { DetectionRecordService } from '../../src/services/detection-record.service';
import { OctavePosition, testConfiguration } from 'symphonova-utils';
import { DetailedDetectionRecord } from '../../src/models/detection-record.model';

describe('DetectionRecordService', () => {
  
  const detectionRecordService = new DetectionRecordService(testConfiguration);

  describe('calculateDetectionRecordDetails', () => {
    describe('positive bin number', () => {
      test('should find same octave', () => {
        const detectionRecordDetails = detectionRecordService.calculateDetectionRecordDetails(3, 524, 8);
        expect(detectionRecordDetails).toEqual({
          frequencyBin: 1,
          octavePosition: OctavePosition.SAME,
        });
      });

      test('should find octave below', () => {
        const detectionRecordDetails = detectionRecordService.calculateDetectionRecordDetails(-42, 20.2, 8);
        expect(detectionRecordDetails).toEqual({
          frequencyBin: 11,
          octavePosition: OctavePosition.BELOW,
        });
      });

      test('should find octave above', () => {
        const detectionRecordDetails = detectionRecordService.calculateDetectionRecordDetails(39, 8450, 8);
        expect(detectionRecordDetails).toEqual({
          frequencyBin: 3,
          octavePosition: OctavePosition.ABOVE,
        });
      });
    });

    describe('negative bin number', () => {
      test('should find same octave', () => {
        const detectionRecordDetails = detectionRecordService.calculateDetectionRecordDetails(13, 910, 8);
        expect(detectionRecordDetails).toEqual({
          frequencyBin: -7,
          octavePosition: OctavePosition.SAME,
        });
      });

      test('should find octave below', () => {
        const detectionRecordDetails = detectionRecordService.calculateDetectionRecordDetails(6, 300, 8);
        expect(detectionRecordDetails).toEqual({
          frequencyBin: -11,
          octavePosition: OctavePosition.BELOW,
        });
      });

      test('should find octave above', () => {
        const detectionRecordDetails = detectionRecordService.calculateDetectionRecordDetails(-21, 250, 8);
        expect(detectionRecordDetails).toEqual({
          frequencyBin: -13,
          octavePosition: OctavePosition.ABOVE,
        });
      });
    });
  });

  describe('calculateRecords', () => {
    test('should parse basic records', () => {

      const input =
        '1, 0. 15 -10;\n' +
        '2, 2.3 20 -20.5;\n' +
        '3, 0. 23 -23.9584;';

      expect(detectionRecordService.parseFileContent(input)).toEqual([
        { frequency: 0, startTime: 15, duration: 5, peak: -10 },
        { frequency: 2.3, startTime: 20, duration: 3, peak: -20.5 },
        { frequency: 0, startTime: 23, duration: 0, peak: -23.9584 },
      ] as DetailedDetectionRecord[]);
    });

    test('should ignore characters after semicolon', () => {
      const input = '1, 0. 15 -10; asd \n' + '2, 2.3 20 -20.5; ';

      expect(detectionRecordService.parseFileContent(input)).toEqual([
        { frequency: 0, startTime: 15, duration: 5, peak: -10 },
        { frequency: 2.3, startTime: 20, duration: 0, peak: -20.5 },
      ] as DetailedDetectionRecord[]);
    });

    test('should ignore unnecessary break lines', () => {
      const input = '1, 0. 15 -10;\n\n\n';

      expect(detectionRecordService.parseFileContent(input)).toEqual([
        {
          frequency: 0,
          startTime: 15,
          duration: 0,
          peak: -10,
        },
      ] as DetailedDetectionRecord[]);
    });
  });
});
