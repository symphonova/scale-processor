import * as path from 'path';
import { readFileSync } from 'fs';
import { ScalesService } from '../../src/services/scales.service';
import { TimeBoundaries, OctavePosition, ScalesConfig, PlayBreakPatternItem, PlayBreakPatternType, testConfiguration, InstrumentConfiguration, ScaleMetadata } from 'symphonova-utils';
import { DetailedDetectionRecord, DetectionRecord } from '../../src/models/detection-record.model';
import { DetectionRecordService } from '../../src/services/detection-record.service';

const samplesDir: string = path.join(__dirname, '../assets/samples');

const basicRecord: DetectionRecord = {
  frequency: 0,
  startTime: 0,
  peak: 0,
  duration: 0
};

const basicDetailedRecord: DetailedDetectionRecord = {
  ...basicRecord,
  frequencyBin: 0,
  octavePosition: OctavePosition.SAME
};

describe('ScalesService', () => {

  const detectionRecordService = new DetectionRecordService(testConfiguration);
  const scalesService = new ScalesService(testConfiguration);

  describe('parseFileNameToScale', () => {
    const basicScale: ScaleMetadata = {
      profileID: '1968',
      instrument: 'Violin',
      string: 'AString',
      dynamicsLevels: 'f',
      articulation: 'ArcoNormale'
    };

    test('Should retrieve metadata from file name', () => {
      const scale = scalesService.parseFileNameToScaleMetadata(
        '1968 Violin AString f ArcoNormale Freq&LevelListing.txt'
      );

      expect(scale).toEqual(basicScale);
    });

    test('Should remove .wav from articulation', () => {
      const scale = scalesService.parseFileNameToScaleMetadata(
        '1968 Violin AString f ArcoNormale.wav Freq&LevelListing.txt'
      );

      expect(scale).toEqual(basicScale);
    });
  });

  describe('calculateGroup', () => {
    test('should return first group for specified duration', () => {
      const input: DetailedDetectionRecord[] = [
        { ...basicDetailedRecord, startTime: 0 },
        { ...basicDetailedRecord, startTime: 1 },
        { ...basicDetailedRecord, startTime: 1 },
        { ...basicDetailedRecord, startTime: 2 },
        { ...basicDetailedRecord, startTime: 3 },
        { ...basicDetailedRecord, startTime: 4 },
        { ...basicDetailedRecord, startTime: 5 },
      ];

      const expected: DetailedDetectionRecord[] = [
        { ...basicDetailedRecord, startTime: 0 },
        { ...basicDetailedRecord, startTime: 1 },
        { ...basicDetailedRecord, startTime: 1 },
      ];
      expect(scalesService.calculateDetectionsGroup(input, 2)).toEqual(expected);
    });

    test('should return group on started startTime', () => {
      const input: DetailedDetectionRecord[] = [
        { ...basicDetailedRecord, startTime: 10 },
        { ...basicDetailedRecord, startTime: 305 },
        { ...basicDetailedRecord, startTime: 309 },
        { ...basicDetailedRecord, startTime: 310 },
        { ...basicDetailedRecord, startTime: 311 },
      ];

      const expected: DetailedDetectionRecord[] = [
        { ...basicDetailedRecord, startTime: 10 },
        { ...basicDetailedRecord, startTime: 305 },
        { ...basicDetailedRecord, startTime: 309 },
      ];
      expect(scalesService.calculateDetectionsGroup(input, 300)).toEqual(expected);
    });
  });

  describe('calculateStartingIndex', () => {
    test('should find starting index for Violin AString f', () => {
      const content = readFileSync(path.join(samplesDir, '1968 Violin AString f ArcoNormale Freq&LevelListing.txt'));
      const fileRecords: DetectionRecord[] = detectionRecordService.parseFileContent(content.toString());
      const startingIndex = scalesService.calculateStartingIndex(
        0,
        fileRecords
      );
      expect(startingIndex).toBe(429);
    });

    test('should stop processing on configured maximum start time', () => {
      const content = readFileSync(path.join(samplesDir, '1968 Violin EString pp ArcoNormale Freq&LevelListing.txt'));
      const fileRecords: DetectionRecord[] = detectionRecordService.parseFileContent(content.toString());
      const startingIndex = scalesService.calculateStartingIndex(7, fileRecords);
      expect(startingIndex).toBe(null);
    });

    test('should handle file with all frequencies 0', () => {
      const content = readFileSync(path.join(samplesDir, '1968 Violin AString f ArcoNormale Freq&LevelListing.txt'));
      const fileRecords: DetectionRecord[] = detectionRecordService.parseFileContent(content.toString()).map(record => ({
        ...record,
        frequency: 0,
      }));
      const startingIndex = scalesService.calculateStartingIndex(
        0,
        fileRecords
      );
      expect(startingIndex).toBe(null);
    });

    test('should not take more than a second for 10,000 lines', () => {
      const startTime = Date.now();
      const fileRecords: DetectionRecord[] = new Array(10000)
        .fill(null)
        .map((_item, index) => ({
          frequency: 0,
          startTime: 20 * index,
          duration: 20,
          peak: 0,
        }));
      scalesService.calculateStartingIndex(0, fileRecords);
      expect(Date.now() - startTime).toBeLessThan(1000);
    });

    test('should handle empty records', () => {
      const startingIndex = scalesService.calculateStartingIndex(0, []);
      expect(startingIndex).toBe(null);
    });
  });

  describe('calculateToneTimes', () => {
    const violinConfig: InstrumentConfiguration = testConfiguration.instruments['Violin'];

    it('should work for basic play break pattern with startTime 0', () => {
      const customConfiguration = calculateCustomConfiguration('Violin', [
        { duration: 1000, type: PlayBreakPatternType.PLAY },
        { duration:  500, type: PlayBreakPatternType.BREAK },
        { duration: 1200, type: PlayBreakPatternType.PLAY }
      ]);
      const customScalesService = new ScalesService(customConfiguration);
      const noteTimes = customScalesService.calculateToneTimes(0, 'Violin', 4);
      expect(noteTimes).toEqual([
        { start: 0,    end: 1000 },
        { start: 1500, end: 2700 },
        { start: 2700, end: 3700 },
        { start: 4200, end: 5400 }
      ]);
    });

    it('should work for basic play break pattern with startTime', () => {
      const customConfiguration = calculateCustomConfiguration('Violin', [
        { duration: 1000, type: PlayBreakPatternType.PLAY },
        { duration: 1000, type: PlayBreakPatternType.BREAK }
      ]);
      const customScalesService = new ScalesService(customConfiguration);
      const noteTimes = customScalesService.calculateToneTimes(500000, 'Violin', 4);
      expect(noteTimes).toEqual([
        { start: 500000, end: 501000 },
        { start: 502000, end: 503000 },
        { start: 504000, end: 505000 },
        { start: 506000, end: 507000 },
      ]);
    });

    it('should work for basic play break pattern with left and right buffers', () => {
      const customConfiguration = calculateCustomConfiguration('Violin', [
        { duration: 1000, type: PlayBreakPatternType.PLAY },
        { duration: 1000, type: PlayBreakPatternType.BREAK }
      ], 400, 200);
      const customScalesService = new ScalesService(customConfiguration);
      const noteTimes = customScalesService.calculateToneTimes(0, 'Violin', 4);
      expect(noteTimes).toEqual([
        { start: 400,  end:  800 },
        { start: 2400, end: 2800 },
        { start: 4400, end: 4800 },
        { start: 6400, end: 6800 }
      ]);
    });

    it('should work with multiple plays and breaks together', () => {
      const customConfiguration = calculateCustomConfiguration('Violin', [
        { duration: 1000, type: PlayBreakPatternType.PLAY },
        { duration: 1500, type: PlayBreakPatternType.PLAY },
        { duration: 1000, type: PlayBreakPatternType.BREAK },
        { duration: 3000, type: PlayBreakPatternType.BREAK },
        { duration: 1500, type: PlayBreakPatternType.PLAY }
      ]);
      const customScalesService = new ScalesService(customConfiguration);
      const noteTimes = customScalesService.calculateToneTimes(0, 'Violin', 4);
      expect(noteTimes).toEqual([
        { start: 0,    end: 1000 },
        { start: 1000, end: 2500 },
        { start: 6500, end: 8000 },
        { start: 8000, end: 9000 }
      ]);
    });

    it('should work with only plays', () => {
      const customConfiguration = calculateCustomConfiguration('Violin', [
        { duration: 1000, type: PlayBreakPatternType.PLAY },
        { duration: 1500, type: PlayBreakPatternType.PLAY },
        { duration: 2000, type: PlayBreakPatternType.PLAY }
      ]);
      const customScalesService = new ScalesService(customConfiguration);
      const noteTimes = customScalesService.calculateToneTimes(0, 'Violin', 4);
      expect(noteTimes).toEqual([
        { start: 0,    end: 1000 },
        { start: 1000, end: 2500 },
        { start: 2500, end: 4500 },
        { start: 4500, end: 5500 }
      ]);
    });

    it('should return no notes when the pattern has only breaks', () => {
      const customConfiguration = calculateCustomConfiguration('Violin', [
        { duration: 1000, type: PlayBreakPatternType.BREAK },
        { duration: 1500, type: PlayBreakPatternType.BREAK }
      ]);
      const customScalesService = new ScalesService(customConfiguration);
      const noteTimes = customScalesService.calculateToneTimes(0, 'Violin', violinConfig.numberOfTones);
      expect(noteTimes.length).toBe(0);
    });

    it('should return no notes when the pattern is empty', () => {
      const customConfiguration = calculateCustomConfiguration('Violin', []);
      const customScalesService = new ScalesService(customConfiguration);
      const noteTimes = customScalesService.calculateToneTimes(0, 'Violin', violinConfig.numberOfTones);
      expect(noteTimes.length).toBe(0);
    });

    function calculateCustomConfiguration(
      instrument: string,
      pattern: PlayBreakPatternItem[],
      timeBufferLeft = 0,
      timeBufferRight = 0
    ): ScalesConfig {
      return {
        ...testConfiguration,
        instruments: {
          ...testConfiguration.instruments,
          [instrument]: {
            ...testConfiguration.instruments[instrument],
            playBreakPattern: pattern,
            timeBufferLeft,
            timeBufferRight
          },
        },
      };
    }
  });

  describe('groupDetectionsRecordsByTone', () => {
    it('should calculate note detections for basic input', () => {
      const records: DetectionRecord[] = [
        { ...basicRecord, startTime: 0 },
        { ...basicRecord, startTime: 10 },
        { ...basicRecord, startTime: 20 },
        { ...basicRecord, startTime: 30 },
      ];
      const boundaries: TimeBoundaries[] = [
        { start: 0, end: 15 },
        { start: 15, end: 30 },
      ];

      const actualNoteDetections = scalesService.groupDetectionsRecordsByTone(
        records,
        boundaries
      );
      expect(actualNoteDetections).toEqual({
        0: [
          { ...basicRecord, startTime: 0 },
          { ...basicRecord, startTime: 10 },
        ],
        1: [{ ...basicRecord, startTime: 20 }],
      });
    });

    it('should calculate note detections for advanced input', () => {
      const records: DetectionRecord[] = [
        { ...basicRecord, startTime: 0 },
        { ...basicRecord, startTime: 3 },
        { ...basicRecord, startTime: 12 },
        { ...basicRecord, startTime: 13 },
        { ...basicRecord, startTime: 14 },
        { ...basicRecord, startTime: 22 },
        { ...basicRecord, startTime: 24 }
      ];
      const boundaries: TimeBoundaries[] = [
        { start: 0, end: 10 },
        { start: 11, end: 12 },
        { start: 20, end: 30 }
      ];

      const actualNoteDetections = scalesService.groupDetectionsRecordsByTone(
        records,
        boundaries
      );
      expect(actualNoteDetections).toEqual({
        0: [
          { ...basicRecord, startTime: 0 },
          { ...basicRecord, startTime: 3 },
        ],
        1: [],
        2: [
          { ...basicRecord, startTime: 22 },
          { ...basicRecord, startTime: 24 },
        ],
      });
    });

    it('should boundaries index as key in result even when not having records', () => {
      const records: DetectionRecord[] = [
        { ...basicRecord, startTime: 1 },
        { ...basicRecord, startTime: 4 },
      ];
      const boundaries: TimeBoundaries[] = [
        { start: 0, end: 2 },
        { start: 2, end: 3 },
        { start: 3, end: 5 }
      ];

      const actualNoteDetections = scalesService.groupDetectionsRecordsByTone(records, boundaries);
      expect(actualNoteDetections).toEqual({
        0: [ { ...basicRecord, startTime: 1 } ],
        1: [],
        2: [ { ...basicRecord, startTime: 4 } ],
      });
    });
  });
});
