import { DetailedDetectionRecord, DetectionRecord } from '../../src/models/detection-record.model';
import { ToneService } from '../../src/services/tone.service';
import { NoteValidationConfiguration, OctavePosition, testConfiguration, TonePeakCorrectness, ToneTuningCorrectness, ValidationConfiguration } from "symphonova-utils";

const basicDetectionRecord: DetectionRecord = {
  frequency: 0,
  startTime: 0,
  duration: 0,
  peak: 0,
};

describe('ToneService', () => {
  const toneService = new ToneService(testConfiguration);

  describe('calculateDetectionRecordsDuration', () => {
    test('should calculate duration for group of records', () => {
      const input: DetectionRecord[] = [
        { frequency: 0, peak: 0, startTime: 500, duration: 1000 },
        { frequency: 0, peak: 0, startTime: 1500, duration: 300 },
      ];

      expect(toneService.calculateDetectionRecordsDuration(input)).toBe(1300);
    });

    test('should return 0 on empty arrays', () => {
      expect(toneService.calculateDetectionRecordsDuration([])).toBe(0);
    });
  });

  describe('calculateTuningCorrectness', () => {
    test('should calculate metadata for provided records', () => {
      const input: DetailedDetectionRecord[] = [
        { frequency: 0, startTime: 100, duration: 10, peak: 0, frequencyBin: 0, octavePosition: OctavePosition.SAME },
        { frequency: 0, startTime: 110, duration: 10, peak: 0, frequencyBin: 0, octavePosition: OctavePosition.BELOW },
        { frequency: 0, startTime: 120, duration: 10, peak: 0, frequencyBin: 0, octavePosition: OctavePosition.ABOVE },
        { frequency: 0, startTime: 130, duration: 10, peak: 0, frequencyBin: null, octavePosition: null },
      ];

      const tuningCorrectness: ToneTuningCorrectness = toneService.calculateTuningCorrectness(input, 40);
      expect(tuningCorrectness).toEqual({
        percentageOfTimeInAllOctaves: 75,
        percentageOfTimeInSameOctave: 25,
        percentageOfTimeInOctaveBelow: 25,
        percentageOfTimeInOctaveAbove: 25,
      });
    });

    test('should calculate metadata with different durations', () => {
      const input: DetailedDetectionRecord[] = [
        { frequency: 440, startTime: 100, duration: 15, peak: 0, frequencyBin: 0, octavePosition: OctavePosition.SAME },
        { frequency: 881, startTime: 115, duration: 5, peak: 0, frequencyBin: 0, octavePosition: OctavePosition.ABOVE },
        { frequency: 219, startTime: 120, duration: 20, peak: 0, frequencyBin: 0, octavePosition: OctavePosition.BELOW },
        { frequency: 50, startTime: 140, duration: 50, peak: 0, frequencyBin: null, octavePosition: null }
      ];

      const tuningCorrectness: ToneTuningCorrectness = toneService.calculateTuningCorrectness(input, 90);
      expect(tuningCorrectness).toEqual({
        percentageOfTimeInAllOctaves: 44.44,
        percentageOfTimeInSameOctave: 16.67,
        percentageOfTimeInOctaveBelow: 22.22,
        percentageOfTimeInOctaveAbove: 5.56,
      });
    });
  });

  describe('calculateDurationErrors', () => {
    const basicDuration = 1001;
    const basicNumberOfDetections = 21;
    const basicNoteValidationConfiguration: NoteValidationConfiguration = {
      minimumDuration: 1000,
      minimumTunedPercentage: 50,
      minimumNumberOfDetections: 20
    };

    it('should throw insufficientDetections error when needed', () => {
      const errors = toneService.calculateDurationErrors(
        basicNoteValidationConfiguration,
        basicDuration,
        19
      );

      expect(errors).toEqual([
        {
          message: testConfiguration.errorMessages.insufficientDetections,
          actualValue: 19,
          minimumValue: 20,
        },
      ]);
    });

    it('should throw insufficientDuration error when needed', () => {
      const errors = toneService.calculateDurationErrors(
        basicNoteValidationConfiguration,
        999,
        basicNumberOfDetections
      );

      expect(errors).toEqual([
        {
          message: testConfiguration.errorMessages.insufficientDuration,
          actualValue: 999,
          minimumValue: 1000,
        },
      ]);
    });

    it('should throw multiple errors together', () => {
      const errors = toneService.calculateDurationErrors(
        basicNoteValidationConfiguration,
        999,
        19
      );

      expect(errors.map(error => error.message)).toEqual([
        testConfiguration.errorMessages.insufficientDetections,
        testConfiguration.errorMessages.insufficientDuration
      ]);
    });

    it('should pass with valid values', () => {
      const errors = toneService.calculateDurationErrors(
        basicNoteValidationConfiguration,
        basicDuration,
        basicNumberOfDetections
      );
      expect(errors).toEqual([]);
    });

    it('should do less and not less or equal', () => {
      const errors = toneService.calculateDurationErrors(
        basicNoteValidationConfiguration,
        1000,
        20
      );

      expect(errors).toEqual([]);
    });
  });

  describe('calculateTuningErrors', () => {

    const basicNoteValidationConfiguration: NoteValidationConfiguration = {
      minimumDuration: 1000,
      minimumTunedPercentage: 50,
      minimumNumberOfDetections: 20
    };
    const basicTuningCorrectness: ToneTuningCorrectness = {
      percentageOfTimeInAllOctaves: 51,
      percentageOfTimeInSameOctave: 100,
      percentageOfTimeInOctaveBelow: 0,
      percentageOfTimeInOctaveAbove: 0,
    };

    it('should throw outOfTune error when needed', () => {
      const tuningCorrectness: ToneTuningCorrectness = {
        ...basicTuningCorrectness,
        percentageOfTimeInAllOctaves: 49,
      };
      const errors = toneService.calculateTuningErrors(basicNoteValidationConfiguration, tuningCorrectness);

      expect(errors).toEqual([{
        message: testConfiguration.errorMessages.outOfTune,
        actualValue: 49,
        minimumValue: 50,
      }]);
    });

    it('should pass with valid values', () => {
      const errors = toneService.calculateTuningErrors(basicNoteValidationConfiguration, basicTuningCorrectness);
      expect(errors).toEqual([]);
    });

    it('should do less and not less or equal', () => {
      const tuningCorrectness: ToneTuningCorrectness = {
        ...basicTuningCorrectness,
        percentageOfTimeInAllOctaves: 50,
      };

      const errors = toneService.calculateTuningErrors(
          basicNoteValidationConfiguration,
          tuningCorrectness
      );

      expect(errors).toEqual([]);
    });

  });

  describe('calculatePeakErrors', () => {

    const basicNoteValidationConfiguration: ValidationConfiguration = {
      minimumDuration: 1000,
      minimumTunedPercentage: 50,
      minimumNumberOfDetections: 20,
      minimumDynamicUniformityPercentage: 80
    };

    it('should throw dynamicsNotUniform error when needed', () => {
      const tonePeakCorrectness: TonePeakCorrectness = {
        percentageOfTimeDynamicallyUniform: 79
      };
      const errors = toneService.calculatePeakErrors(basicNoteValidationConfiguration, tonePeakCorrectness);

      expect(errors).toEqual([{
        message: testConfiguration.errorMessages.dynamicsNotUniform,
        actualValue: 79,
        minimumValue: 80
      }]);
    });

    it('should pass with valid values', () => {
      const tonePeakCorrectness: TonePeakCorrectness = {
        percentageOfTimeDynamicallyUniform: 81
      };
      const errors = toneService.calculatePeakErrors(basicNoteValidationConfiguration, tonePeakCorrectness);

      expect(errors).toEqual([]);
    });

    it('should do less and not less or equal', () => {
      const tonePeakCorrectness: TonePeakCorrectness = {
        percentageOfTimeDynamicallyUniform: 80
      };
      const errors = toneService.calculatePeakErrors(basicNoteValidationConfiguration, tonePeakCorrectness);

      expect(errors).toEqual([]);
    });

  });

  describe('calculateMostPopularIntPeakSet', () => {
    it('should catch peak of 1 consecutive number', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet(
        convertPeaksToRecords(1, 5, 9)
      );

      expect(peakSet).toEqual({
        set: [1],
        appearances: 1,
        duration: 0,
      });
    });

    it('should catch peak of 2 consecutive numbers', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet(
        convertPeaksToRecords(1, 2, 7, 9)
      );

      expect(peakSet).toEqual({
        set: [1, 2],
        appearances: 2,
        duration: 0,
      });
    });

    it('should catch peak until 4 consecutive numbers', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet(
        convertPeaksToRecords(-2, -1, 0, 1, 2, 3, 7, 9)
      );

      expect(peakSet).toEqual({
        set: [-2, -1, 0, 1],
        appearances: 4,
        duration: 0,
      });
    });

    it('should handle empty input', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet([]);
      expect(peakSet).toEqual(null);
    });

    it('should handle multiple appearances of the same numbers', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet(
        convertPeaksToRecords(1, 1, 2, 2, 3, 4, 5, 6, 7, 77)
      );

      expect(peakSet).toEqual({
        set: [1, 2, 3, 4],
        appearances: 6,
        duration: 0,
      });
    });

    it('should choose between different candidates', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet(
        convertPeaksToRecords(1, 2, 2, 3, 4, 5, 30, 30, 30, 30, 31, 32)
      );

      expect(peakSet).toEqual({
        set: [30, 31, 32],
        appearances: 6,
        duration: 0,
      });
    });

    it('should round provided peaks', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet(
        convertPeaksToRecords(1.1, 2.2, 3.5)
      );

      expect(peakSet).toEqual({
        set: [1, 2],
        appearances: 2,
        duration: 0,
      });
    });

    it('should choose first candidate when multiple have the same number of appearances', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet(
        convertPeaksToRecords(1, 2, 5, 6)
      );

      expect(peakSet).toEqual({
        set: [1, 2],
        appearances: 2,
        duration: 0,
      });
    });

    it('should sort provided peaks', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet(
        convertPeaksToRecords(5, 2, 7, 1)
      );

      expect(peakSet).toEqual({
        set: [1, 2],
        appearances: 2,
        duration: 0,
      });
    });

    it('should calculate peak set duration', () => {
      const peakSet = toneService.calculateMostPopularIntPeakSet([
        { ...basicDetectionRecord, peak: 1, duration: 100 },
        { ...basicDetectionRecord, peak: 2, duration: 110 },
        { ...basicDetectionRecord, peak: 3, duration: 120 },
        { ...basicDetectionRecord, peak: 6, duration: 130 },
      ]);

      expect(peakSet).toEqual({
        set: [1, 2, 3],
        appearances: 3,
        duration: 330,
      });
    });

    function convertPeaksToRecords(...peakList: number[]): DetectionRecord[] {
      return peakList.map(peak => ({
        ...basicDetectionRecord,
        peak,
      }));
    }
  });
});
