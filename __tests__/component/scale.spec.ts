import * as path from 'path';
import { readFileSync } from 'fs';
import { ScaleFactory } from '../../src/factories/scale.factory';
import { readFilesInDirectory, ScaleMetadata, testConfiguration, Tone } from 'symphonova-utils';
import { filter, toArray } from "rxjs/operators";
import { DetectionRecord } from '../../src/models/detection-record.model';
import { ScalesService } from '../../src/services/scales.service';
import { DetectionRecordService } from '../../src/services/detection-record.service';

const samplesDir: string = path.join(__dirname, '../assets/samples');

describe('Component Scale Tests', () => {
	
	const scaleFactory = new ScaleFactory(testConfiguration);
	const scalesService = new ScalesService(testConfiguration);
	const detectionRecordService = new DetectionRecordService(testConfiguration);

	test('All samples except for EString pp should have 49 tones', () => {
		return new Promise(done => {
			readFilesInDirectory(samplesDir).pipe(
				filter(({ name }) => !name.includes('EString pp')),
				toArray()
			).subscribe(
				files => {
					const expected = files.reduce((expected, { name }) => ({
						...expected,
						[name]: 49
					}), {});

					const actual = files.reduce((actual, { name, content }) => ({
						...actual,
						[name]: calculateTones(name).length
					}), {});

					expect(actual).toEqual(expected);
					done();
				}
			);
		})
	});

	test('Should parse Violin EString pp to scale without tones', () => {
		const fileName = '1968 Violin EString pp ArcoNormale Freq&LevelListing.txt';
		const tones: Tone[] = calculateTones(fileName);

		expect(tones.length).toEqual(0);
	});

	test('Should match random note from DString ff expected values', () => {
		const fileName = '1968 Violin DString ff ArcoNormale Freq&LevelListing.txt';
		const tones: Tone[] = calculateTones(fileName);

		expect(tones[0].startTime).toEqual(2770);
		expect(tones[33]).toEqual({
			noteName: 'F5',
			noteCentralFrequency: 698.46,
			tonePosition: 33,
			tunedDuration: 405,
			totalDuration: 405,
			peakMean: -27.32,
			peakRMS: 27.36,
			score: 92.22,
			startTime: 68777,
			numberOfDetections: 19,
			tuningCorrectness: {
				percentageOfTimeInAllOctaves: 100,
				percentageOfTimeInSameOctave: 78.77,
				percentageOfTimeInOctaveBelow: 21.23,
				percentageOfTimeInOctaveAbove: 0
			},
			peakCorrectness: {
				percentageOfTimeDynamicallyUniform: 84.44
			},
			toneErrors: []
		});
	});

	test('Should match random note from DString pp expected values', () => {
		const fileName = '1968 Violin DString pp ArcoNormale Freq&LevelListing.txt';
		const tones: Tone[] = calculateTones(fileName);

		expect(tones[0].startTime).toEqual(1329);
		expect(tones[19]).toEqual({
			noteName: 'A5',
			noteCentralFrequency: 880,
			tonePosition: 19,
			tunedDuration: 192,
			totalDuration: 385,
			peakMean: -51.24,
			peakRMS: 51.26,
			score: 69.21,
			startTime: 39345,
			numberOfDetections: 16,
			tuningCorrectness: {
				percentageOfTimeInAllOctaves: 49.87,
				percentageOfTimeInSameOctave: 44.16,
				percentageOfTimeInOctaveBelow: 5.71,
				percentageOfTimeInOctaveAbove: 0
			},
			peakCorrectness: {
				percentageOfTimeDynamicallyUniform: 88.54
			},
			toneErrors: [{
				message: testConfiguration.errorMessages.outOfTune,
				actualValue: 49.87,
				minimumValue: 90
			}]
		});
	});
	
	test('Should handle tone with no tuned record detections', () => {
		const fileName = '1968 Violin GString pp ArcoNormale Freq&LevelListing.txt';
		const tones: Tone[] = calculateTones(fileName);

		expect(tones[6]).toEqual({
			noteName: 'C#4',
			noteCentralFrequency: 277.18,
			tonePosition: 6,
			tunedDuration: 0,
			totalDuration: 384,
			peakMean: null,
			peakRMS: null,
			score: 0,
			startTime: 14262,
			numberOfDetections: 18,
			tuningCorrectness: {
				percentageOfTimeInAllOctaves: 0,
				percentageOfTimeInSameOctave: 0,
				percentageOfTimeInOctaveBelow: 0,
				percentageOfTimeInOctaveAbove: 0
			},
			peakCorrectness: {
				percentageOfTimeDynamicallyUniform: 0
			},
			toneErrors: [{
				message: testConfiguration.errorMessages.outOfTune,
				actualValue: 0,
				minimumValue: 90
			}, {
				message: testConfiguration.errorMessages.dynamicsNotUniform,
				actualValue: 0,
				minimumValue: 80
			}]
		});
	});
	
	test('Should match random note from AString mf expected values', () => {
		const fileName = '1968 Violin AString mf ArcoNormale Freq&LevelListing.txt';
		const tones: Tone[] = calculateTones(fileName);

		expect(tones[0].startTime).toEqual(1268);
		expect(tones[25]).toEqual({
			noteName: 'G#6',
			noteCentralFrequency: 1661.22,
			tonePosition: 25,
			tunedDuration: 236,
			totalDuration: 406,
			peakMean: -34.85,
			peakRMS: 34.89,
			score: 74.41,
			startTime: 51274,
			numberOfDetections: 19,
			tuningCorrectness: {
				percentageOfTimeInAllOctaves: 58.13,
				percentageOfTimeInSameOctave: 58.13,
				percentageOfTimeInOctaveBelow: 0,
				percentageOfTimeInOctaveAbove: 0
			},
			peakCorrectness: {
				percentageOfTimeDynamicallyUniform: 90.68
			},
			toneErrors: [{
				message: testConfiguration.errorMessages.outOfTune,
				actualValue: 58.13,
				minimumValue: 90
			}]
		});
	});

	function calculateTones(fileName: string): Tone[] {
		const scale: ScaleMetadata = scalesService.parseFileNameToScaleMetadata(fileName);
		const fileContent: string = readFileSync(path.join(samplesDir, fileName)).toString();
		const detectionRecords: DetectionRecord[] = detectionRecordService.parseFileContent(fileContent);
		return scaleFactory.calculateTones(scale, detectionRecords, []);
	}

});
