import * as path from 'path';
import { readFileSync } from 'fs';
import { ScaleFactory } from '../../src/factories/scale.factory';
import { InstrumentConfiguration, NoteError, PlayBreakPatternType, ScaleMetadata, ScalesConfig, testConfiguration, Tone } from 'symphonova-utils';
import { DetectionRecord } from '../../src/models/detection-record.model';
import { ScalesService } from '../../src/services/scales.service';
import { DetectionRecordService } from '../../src/services/detection-record.service';
import { pick } from 'lodash';

const correctionsDir: string = path.join(__dirname, '../assets/corrections');

describe('Component Correction Tests', () => {

    test('Should correct note C4 and C5', () => {
        const fileName = '1968 Viola GString f ScaleA Freq&LevelListing C4C5.txt';
        const customScalesConfig: ScalesConfig = getCustomScalesConfig(23000);
		const tones: Tone[] = calculateTones(fileName, [
            { errorMessages: [ 'Note out of tune' ], noteName: 'C4' },
            { errorMessages: [ 'Note out of tune' ], noteName: 'C5' }
        ], customScalesConfig);

        expect(tones.map(tone => pick(tone, 'noteName', 'score', 'toneErrors'))).toEqual([
            { noteName: 'C4', score: 100, toneErrors: [] },
            { noteName: 'C5', score: 100, toneErrors: [] }
        ]);
    });
    
    test('Should correct note C4 and C5, F4 should error', () => {
        const fileName = '1968 Viola GString f ScaleB Freq&LevelListing C4F4C5.txt';
        const customScalesConfig: ScalesConfig = getCustomScalesConfig(9000, 13000);
		const tones: Tone[] = calculateTones(fileName, [
            { errorMessages: [ 'Note out of tune' ], noteName: 'C4' },
            { errorMessages: [ 'Note out of tune' ], noteName: 'F4' },
            { errorMessages: [ 'Note out of tune' ], noteName: 'C5' }
        ], customScalesConfig);

        expect(tones.map(tone => pick(tone, 'noteName', 'score', 'toneErrors'))).toEqual([
            { noteName: 'C4', score: 100, toneErrors: [] },
            { noteName: 'F4', score: 0, toneErrors: [
                { actualValue: 0, message: 'Note out of tune', minimumValue: 90 },
                { actualValue: 0, message: 'Dynamics are not uniform', minimumValue: 80 }
            ] },
            { noteName: 'C5', score: 100, toneErrors: [] }
        ]);
	});
    
    test('Should handle incorrect break time', () => {
        const fileName = '1968 Viola GString f ScaleB Freq&LevelListing C4F4C5.txt';
        const customScalesConfig: ScalesConfig = getCustomScalesConfig(10000, 13000);
		const tones: Tone[] = calculateTones(fileName, [
            { errorMessages: [ 'Note out of tune' ], noteName: 'C4' },
            { errorMessages: [ 'Note out of tune' ], noteName: 'F4' },
            { errorMessages: [ 'Note out of tune' ], noteName: 'C5' }
        ], customScalesConfig);

        expect(tones[1]).toEqual({
            noteCentralFrequency: 349.23,
            noteName: 'F4',
            numberOfDetections: 0,
            peakCorrectness: {
                percentageOfTimeDynamicallyUniform: 0
            },
            peakMean: null,
            peakRMS: null,
            score: 0,
            startTime: null,
            toneErrors: [
                { actualValue: 0, message: 'Not enough detections in note', minimumValue: 13 },
                { actualValue: 0, message: 'Note detection period too short', minimumValue: 280 },
                { actualValue: 0, message: 'Note out of tune', minimumValue: 90 },
                { actualValue: 0, message: 'Dynamics are not uniform', minimumValue: 80 }
            
            ],
            tonePosition: 1,
            totalDuration: 0,
            tunedDuration: 0,
            tuningCorrectness: {
                percentageOfTimeInAllOctaves: 0,
                percentageOfTimeInOctaveAbove: 0,
                percentageOfTimeInOctaveBelow: 0,
                percentageOfTimeInSameOctave: 0
            }
        });
	});
    
    test('Should correct note F4', () => {
        const fileName = '1968 Viola GString f ScaleB Freq&LevelListing F4.txt';
        const customScalesConfig: ScalesConfig = getCustomScalesConfig(1000);
		const tones: Tone[] = calculateTones(fileName, [
            { errorMessages: [ 'Note out of tune' ], noteName: 'F4' }
        ], customScalesConfig);

        expect(tones.map(tone => pick(tone, 'noteName', 'score', 'toneErrors'))).toEqual([
            { noteName: 'F4', score: 100, toneErrors: [] }
        ]);
	});

	function calculateTones(fileName: string, latestsNoteErrors: NoteError[], scalesConfig: ScalesConfig): Tone[] {
        const scaleFactory = new ScaleFactory(scalesConfig);
        const scalesService = new ScalesService(scalesConfig);
        const detectionRecordService = new DetectionRecordService(scalesConfig);
    
		const scale: ScaleMetadata = scalesService.parseFileNameToScaleMetadata(fileName);
		const fileContent: string = readFileSync(path.join(correctionsDir, fileName)).toString();
		const detectionRecords: DetectionRecord[] = detectionRecordService.parseFileContent(fileContent);
		return scaleFactory.calculateTones(scale, detectionRecords, latestsNoteErrors);
    }
    
    function getCustomScalesConfig(...breaks: number[]): ScalesConfig {
        const playBreakPattern: InstrumentConfiguration['playBreakPattern'] =
            breaks.reduce((acc, breakDuration) => {
                return [
                    ...acc,
                    { duration: 1000, type: PlayBreakPatternType.PLAY },
                    { duration: breakDuration, type: PlayBreakPatternType.BREAK }
                ];
            }, [] as InstrumentConfiguration['playBreakPattern']);

        return {
            ...testConfiguration,
            instruments: {
                ...testConfiguration.instruments,
                Viola: {
                    startingNotePerString: { CString: -21, GString: -14, DString: -7, AString: 0 },
                    numberOfTones: 49,
                    timeBufferLeft: 400,
                    timeBufferRight: 200,
                    binDivisionsPerSemiTone: 8,
                    binDelimiters: { left: -15, right: 15 },
                    playBreakPattern
                }
            }
        }
    }

});
