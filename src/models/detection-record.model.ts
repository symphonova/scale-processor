import { OctavePosition } from "symphonova-utils";

export interface DetectionRecord {
	frequency: number;
	peak: number;
	startTime: number;
	duration: number;
}

export interface DetectionRecordDetails {
	frequencyBin: number | null;
	octavePosition: OctavePosition | null;
}

export type DetailedDetectionRecord = DetectionRecord & DetectionRecordDetails;
