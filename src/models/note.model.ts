export interface FrequencyBoundaries {
	min: number;
	max: number;
}
