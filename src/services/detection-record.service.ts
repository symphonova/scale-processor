import { inRange } from 'lodash';
import { DetectionRecord, DetectionRecordDetails } from '../models/detection-record.model';
import { ScalesConfig, NoteHelper, OctavePosition } from 'symphonova-utils';

export class DetectionRecordService {
	private notesHelper: NoteHelper;
	private octavePositions: OctavePosition[] = [ OctavePosition.SAME, OctavePosition.BELOW, OctavePosition.ABOVE, ];

	constructor(config: ScalesConfig) {
		this.notesHelper = new NoteHelper(config);
	}

	public parseFileContent(fileContent: string): DetectionRecord[] {
		const lines = fileContent.split('\n').filter(line => !!line);
		const records: DetectionRecord[] = [ this.parseDetectionRecordLine(lines[0]) ];
	
		for (let i = 1; i < lines.length; i++) {
			const record: DetectionRecord = this.parseDetectionRecordLine(lines[i]);
			const previousRecord: DetectionRecord = records[i - 1];
			previousRecord.duration = record.startTime - previousRecord.startTime;
			records.push(record);
		}
	
		return records;
	}

	public calculateDetectionRecordDetails(noteKey: number, playedFrequency: number, binDivisionsPerSemiTone: number): DetectionRecordDetails {
		const intervals = {
			[OctavePosition.SAME]: this.calculateNoteIntervals(noteKey + OctavePosition.SAME, binDivisionsPerSemiTone),
			[OctavePosition.BELOW]: this.calculateNoteIntervals(noteKey + OctavePosition.BELOW, binDivisionsPerSemiTone),
			[OctavePosition.ABOVE]: this.calculateNoteIntervals(noteKey + OctavePosition.ABOVE, binDivisionsPerSemiTone),
		};

		const lowestBin = -binDivisionsPerSemiTone * 2 + 1;
		const highestBin = binDivisionsPerSemiTone * 2 - 1;
		for (let binNumber = lowestBin; binNumber < highestBin; binNumber += 2) {
			if (binNumber < 0) {
				const step: number = (binNumber - 1) / 2;

				for (const octavePosition of this.octavePositions) {
					if (inRange(playedFrequency, intervals[octavePosition].noteFrequency + intervals[octavePosition].lower * step, intervals[octavePosition].noteFrequency + intervals[octavePosition].lower * (step + 1))) {
						return { frequencyBin: binNumber, octavePosition };
					}
				}
			} else if (binNumber > 0) {
				const step: number = (binNumber + 1) / 2;

				for (const octavePosition of this.octavePositions) {
					if (inRange(playedFrequency, intervals[octavePosition].noteFrequency + intervals[octavePosition].upper * (step - 1), intervals[octavePosition].noteFrequency + intervals[octavePosition].upper * step)) {
						return { frequencyBin: binNumber, octavePosition };
					}
				}
			}
		}

		return { frequencyBin: null, octavePosition: null };
	}

	private calculateNoteIntervals(noteKey: number, binDivisionsPerSemiTone: number): { noteFrequency: number; upper: number; lower: number } {
		const noteFrequency: number = this.notesHelper.calculateFrequency(noteKey);
		return {
			noteFrequency, upper: (this.notesHelper.calculateFrequency(noteKey + 1) - noteFrequency) / binDivisionsPerSemiTone,
			lower: (noteFrequency - this.notesHelper.calculateFrequency(noteKey - 1)) / binDivisionsPerSemiTone,
		};
	}

	private parseDetectionRecordLine(line: string): DetectionRecord {
		const lineParts = line.split(';')[0].split(' ');
		return {
			frequency: Number(lineParts[1]), startTime: Number(lineParts[2]), peak: Number(lineParts[3]), duration: 0,
		};
	}
}
