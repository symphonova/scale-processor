import { BinDelimitersConfiguration, NoteValidationConfiguration, OctavePosition, PeakSet, ScalesConfig, ToneError, TonePeakCorrectness, ToneTuningCorrectness, ValidationConfiguration } from 'symphonova-utils';
import { DetailedDetectionRecord, DetectionRecord, } from '../models/detection-record.model';
import { maxBy, mean, round, inRange, sumBy } from 'lodash';

export class ToneService {

	constructor(private config: ScalesConfig) {}

	public calculateTuningCorrectness(records: DetailedDetectionRecord[], totalDuration: number): ToneTuningCorrectness {
		let octaveSameDuration = 0;
		let octaveBelowDuration = 0;
		let octaveAboveDuration = 0;

		records.forEach(record => {
			switch (record.octavePosition) {
				case OctavePosition.SAME:
					octaveSameDuration += record.duration;
					break;
				case OctavePosition.BELOW:
					octaveBelowDuration += record.duration;
					break;
				case OctavePosition.ABOVE:
					octaveAboveDuration += record.duration;
					break;
			}
		});

		const tunedDuration: number = octaveSameDuration + octaveAboveDuration + octaveBelowDuration;
		return {
			percentageOfTimeInAllOctaves: round(tunedDuration / (totalDuration || 1) * 100, 2),
			percentageOfTimeInSameOctave: round(octaveSameDuration / (totalDuration || 1) * 100, 2),
			percentageOfTimeInOctaveBelow: round(octaveBelowDuration / (totalDuration || 1) * 100, 2),
			percentageOfTimeInOctaveAbove: round(octaveAboveDuration / (totalDuration || 1) * 100, 2)
		};
	}

	public calculatePeakCorrectness(records: DetectionRecord[]): TonePeakCorrectness {
		const totalRecordsDuration = sumBy(records, record => record.duration);
		const mostPopularIntPeakSet: PeakSet | null = this.calculateMostPopularIntPeakSet(records);

		const percentageOfTimeDynamicallyUniform = mostPopularIntPeakSet === null ?
			0 :
			round(mostPopularIntPeakSet.duration / totalRecordsDuration * 100, 2);

		return { percentageOfTimeDynamicallyUniform };
	}

	public calculateDurationErrors(thresholds: NoteValidationConfiguration, duration: number, numberOfDetections: number): ToneError[] {
		const toneErrors: ToneError[] = [];

		if (numberOfDetections < thresholds.minimumNumberOfDetections) {
			toneErrors.push({
				message: this.config.errorMessages.insufficientDetections,
				actualValue: numberOfDetections,
				minimumValue: thresholds.minimumNumberOfDetections
			});
		}
		if (duration < thresholds.minimumDuration) {
			toneErrors.push({
				message: this.config.errorMessages.insufficientDuration,
				actualValue: duration,
				minimumValue: thresholds.minimumDuration
			});
		}

		return toneErrors;
	}

	public calculateTuningErrors(thresholds: NoteValidationConfiguration, tuningCorrectness: ToneTuningCorrectness): ToneError[] {
		const toneErrors: ToneError[] = [];
		if (tuningCorrectness.percentageOfTimeInAllOctaves < thresholds.minimumTunedPercentage) {
			toneErrors.push({
				message: this.config.errorMessages.outOfTune,
				actualValue: tuningCorrectness.percentageOfTimeInAllOctaves,
				minimumValue: thresholds.minimumTunedPercentage
			});
		}
		return toneErrors;
	}

	public calculatePeakErrors(thresholds: ValidationConfiguration, peakCorrectness: TonePeakCorrectness): ToneError[] {
		const toneErrors: ToneError[] = [];
		if (peakCorrectness.percentageOfTimeDynamicallyUniform < thresholds.minimumDynamicUniformityPercentage) {
			toneErrors.push({
				message: this.config.errorMessages.dynamicsNotUniform,
				actualValue: peakCorrectness.percentageOfTimeDynamicallyUniform,
				minimumValue: thresholds.minimumDynamicUniformityPercentage
			});
		}
		return toneErrors;
	}

	public calculateScore(tuningCorrectness: ToneTuningCorrectness, peakCorrectness: TonePeakCorrectness): number {
		const score: number = mean([
			tuningCorrectness.percentageOfTimeInAllOctaves,
			peakCorrectness.percentageOfTimeDynamicallyUniform
		]);
		return round(score, 2);
	}

	public calculateDetectionRecordsDuration(records: DetectionRecord[]): number {
		if (records.length === 0) {
			return 0;
		}

		const startTime = records[0].startTime;
		const endTime = records[records.length - 1].startTime + records[records.length - 1].duration;
		return endTime - startTime;
	}

	public calculateMostPopularIntPeakSet(records: DetectionRecord[]): PeakSet | null {
		const recordPeaks: number[] = records.map(record => Math.round(record.peak));
		const peaksSet: number[] = [ ...new Set(recordPeaks) ].sort((a, b) => a - b);

		const consecutivePeaks: number[][] = [];

		peaksSet.forEach(peak => {
			for (let maxPeak = 3; maxPeak >= 0; maxPeak--) {
				const nextPeaks = new Array(maxPeak).fill(null).map((_peak, index) => peak + index + 1);
				if (nextPeaks.every(val => peaksSet.includes(val))) {
					consecutivePeaks.push([ peak, ...nextPeaks ]);
				}
			}
		});

		const peakSetList: PeakSet[] = [];
		consecutivePeaks.forEach(peakGroup => {
			const peakSet: PeakSet = {
				set: peakGroup, appearances: 0, duration: 0,
			};

			recordPeaks.forEach((peak, recordPeakIndex) => {
				if (peakGroup.includes(peak)) {
					peakSet.appearances += 1;
					peakSet.duration += records[recordPeakIndex].duration;
				}
			});

			peakSetList.push(peakSet);
		});

		return maxBy(peakSetList, item => item.appearances) || null;
	}

	public areRecordsInValidBin(binDelimiters: BinDelimitersConfiguration): (frequencyBin: number | null) => boolean {
		return frequencyBin => {
			return frequencyBin !== null &&
			       inRange(frequencyBin, binDelimiters.left, binDelimiters.right + 1);
		}
	}
}
