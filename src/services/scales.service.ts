import { inRange, takeWhile } from 'lodash';
import * as path from 'path';
import { ToneService } from './tone.service';
import { DetailedDetectionRecord, DetectionRecord, } from '../models/detection-record.model';
import { DetectionRecordService } from './detection-record.service';
import { ScalesConfig, PlayBreakPatternType, log, ScaleMetadata, Dictionary,
	TimeBoundaries, ToneError, ToneTuningCorrectness, validateScaleMetadata, InstrumentConfiguration } from 'symphonova-utils';

interface TimeBoundariesWithType extends TimeBoundaries {
	type: PlayBreakPatternType;
}

export class ScalesService {
	private detectionRecordService: DetectionRecordService;
	private toneService: ToneService;

	public constructor(private config: ScalesConfig) {
		this.detectionRecordService = new DetectionRecordService(config);
		this.toneService = new ToneService(config);
	}

	public calculateDetectionsGroup(records: DetailedDetectionRecord[], duration: number): DetailedDetectionRecord[] {
		const startTime = records[0].startTime;
		return takeWhile(records, record => record.startTime < startTime + duration);
	}

	public calculateStartingIndex(noteKey: number, records: DetectionRecord[]): number | null {
		let startingIndex: number | null = null;

		const recordsInRange: DetectionRecord[] = takeWhile(records, record => {
			return record.startTime < this.config.firstIndexDetection.maxStartTime
		});
		const detailedRecordsInRange: DetailedDetectionRecord[] = this.convertDetectionRecordsToDetailed(
			recordsInRange, noteKey, this.config.firstIndexDetection.binDivisionsPerSemiTone
		);

		detailedRecordsInRange.some((_record, recordIndex) => {
			const detectionsGroup: DetailedDetectionRecord[] = this.calculateDetectionsGroup(detailedRecordsInRange.slice(recordIndex), this.config.firstIndexDetection.expectedDuration);

			const totalDuration: number = this.toneService.calculateDetectionRecordsDuration(detectionsGroup);
			const isRecordInTune = this.toneService.areRecordsInValidBin(this.config.firstIndexDetection.binDelimiters);
			const tunedRecords: DetailedDetectionRecord[] = detectionsGroup.filter(record => isRecordInTune(record.frequencyBin));
			
			const toneTuningCorrectness: ToneTuningCorrectness = this.toneService.calculateTuningCorrectness(tunedRecords, totalDuration);
			const noteDetectionsErrors: ToneError[] = [
				...this.toneService.calculateDurationErrors(this.config.firstIndexDetection.noteValidation, totalDuration, detectionsGroup.length),
				...this.toneService.calculateTuningErrors(this.config.firstIndexDetection.noteValidation, toneTuningCorrectness)
			];

			const isNoteDetectionValid: boolean = noteDetectionsErrors.length === 0;
			
			if (isNoteDetectionValid) {
				const firstValidNoteDetectionIndex = detectionsGroup.findIndex(groupDetection => {
					const isValidStartTime = groupDetection.startTime < this.config.firstIndexDetection.maxStartTime;
					const isInValidBin = isRecordInTune(groupDetection.frequencyBin);

					return isValidStartTime && isInValidBin;
				});

				if (firstValidNoteDetectionIndex >= 0) {
					startingIndex = recordIndex + firstValidNoteDetectionIndex;
				}
			}

			return isNoteDetectionValid;
		});

		if (startingIndex === null) {
			log.warn('First note not detected');
			const lastDetection: DetectionRecord = recordsInRange[recordsInRange.length - 1];
			if (lastDetection) {
				log.warn(`Stopped searching at detection ${recordsInRange.length} with start time ${lastDetection.startTime}`);
			} else {
				log.warn('Provided scale was empty');
			}
		}

		return startingIndex;
	}

	public calculateToneTimes(startTime: number, instrument: string, numberOfTones: number): TimeBoundaries[] {
		const instrumentConfig: InstrumentConfiguration = this.config.instruments[instrument];
		const numberOfPlaysInPattern = instrumentConfig.playBreakPattern.filter(item => item.type === PlayBreakPatternType.PLAY);
		
		if (numberOfPlaysInPattern.length === 0) {
			log.warn(`No detected plays in the configured play break pattern`);
			return [];
		}

		const allItems: TimeBoundariesWithType[] = [];
		let numberOfPlays = 0;
		for (let index = 0; numberOfPlays < numberOfTones; index++) {
			const currentItem = instrumentConfig.playBreakPattern[index % instrumentConfig.playBreakPattern.length];
			const noteStartTime = allItems.length > 0 ? allItems[allItems.length - 1].end + instrumentConfig.timeBufferRight : startTime;
			allItems.push({
				start: noteStartTime + instrumentConfig.timeBufferLeft, end: noteStartTime + currentItem.duration - instrumentConfig.timeBufferRight,
				type: currentItem.type,
			});
			if (currentItem.type === PlayBreakPatternType.PLAY) {
				numberOfPlays++;
			}
		}

		return allItems.filter(item => item.type === PlayBreakPatternType.PLAY).map(item => ({
			start: item.start, end: item.end,
		}));
	}

	public groupDetectionsRecordsByTone(
		records: DetectionRecord[],
		boundaries: TimeBoundaries[]
	): Dictionary<DetectionRecord[]> {
		const groups: Dictionary<DetectionRecord[]> = boundaries.reduce((acc, boundary, index) => ({
			...acc,
			[index]: []
		}), {});
		let boundariesIndex = 0;

		for (let recordsIndex = 0; recordsIndex < records.length && boundaries[boundariesIndex]; recordsIndex++) {
			const record = records[recordsIndex];

			let foundBoundary;
			while (!foundBoundary && boundariesIndex < boundaries.length && boundaries[boundariesIndex].start <= record.startTime) {
				if (inRange(record.startTime, boundaries[boundariesIndex].start, boundaries[boundariesIndex].end)) {
					foundBoundary = true;
				} else {
					boundariesIndex++;
				}
			}

			if (foundBoundary) {
				groups[boundariesIndex].push(record);
			}
		}

		return groups;
	}

	public parseFileNameToScaleMetadata(fileName: string): ScaleMetadata {
		const nameParts = fileName.split(' ');
		const metadata: unknown = {
			profileID: nameParts[0],
			instrument: nameParts[1],
			string: nameParts[2],
			dynamicsLevels: nameParts[3],
			articulation: path.parse(nameParts[4]).name
		};

		return validateScaleMetadata(metadata);
	}

	public convertDetectionRecordsToDetailed(records: DetectionRecord[], noteKey: number, binDivisionsPerSemiTone: number): DetailedDetectionRecord[] {
		const detailedRecords: DetailedDetectionRecord[] = [];

		records.forEach(record => {
			const details = this.detectionRecordService.calculateDetectionRecordDetails(noteKey, record.frequency, binDivisionsPerSemiTone);

			if (details) {
				detailedRecords.push({ ...record, ...details });
			}
		});

		return detailedRecords;
	}
}
