import { ToneFactory } from './tone.factory';
import { ScalesService } from '../services/scales.service';
import { DetectionRecord, } from '../models/detection-record.model';
import { Dictionary, ScalesConfig, NoteHelper, NoteError, ScaleMetadata, Tone, TimeBoundaries, InstrumentConfiguration } from 'symphonova-utils';

export class ScaleFactory {
	
	private notesHelper: NoteHelper;
	private scalesService: ScalesService;
	private toneFactory: ToneFactory;

	constructor(private scalesConfig: ScalesConfig) {
		this.notesHelper = new NoteHelper(scalesConfig);
		this.scalesService = new ScalesService(scalesConfig);
		this.toneFactory = new ToneFactory(scalesConfig);
	}

	public calculateTones(
		scale: ScaleMetadata,
		detectionRecords: DetectionRecord[],
		latestNoteErrors: NoteError[]
	): Tone[] {
		const initialNoteKey: number = this.getInitialNoteKey(scale, latestNoteErrors);
		const startingNoteIndex = this.scalesService.calculateStartingIndex(initialNoteKey, detectionRecords);

		if (startingNoteIndex === null) {
			return [];
		}
		
		
		const instrumentConfig: InstrumentConfiguration = this.scalesConfig.instruments[scale.instrument];
		const expectedNumberOfTones: number = latestNoteErrors.length || instrumentConfig.numberOfTones;
		const expectedToneTimes: TimeBoundaries[] = this.scalesService.calculateToneTimes(
			detectionRecords[startingNoteIndex].startTime,
			scale.instrument,
			expectedNumberOfTones
		);
		const detectionRecordsByTone: Dictionary<DetectionRecord[]> = this.scalesService.groupDetectionsRecordsByTone(
			detectionRecords,
			expectedToneTimes
		);
		const instrumentConfiguration = this.scalesConfig.instruments[scale.instrument];

		return Object.entries(detectionRecordsByTone).map(([ key, records ]) => {
			const tonePosition = Number(key);
			const noteKey: number = this.getExpectedNoteKey(scale.instrument, tonePosition, initialNoteKey, latestNoteErrors);
			const detailedRecords = this.scalesService.convertDetectionRecordsToDetailed(records, noteKey, instrumentConfiguration.binDivisionsPerSemiTone);

			return this.toneFactory.fromDetailedDetectionRecords(noteKey, tonePosition, detailedRecords, instrumentConfiguration.binDelimiters);
		});
	}

	private getInitialNoteKey(scale: ScaleMetadata, latestNoteErrors: NoteError[]): number {
		if (latestNoteErrors.length > 0) {
			return this.notesHelper.getNoteKey(latestNoteErrors[0].noteName);
		} else {
			return this.notesHelper.getScaleStartingNoteKey(scale.instrument, scale.string);
		}
	}

	private getExpectedNoteKey(
		instrument: string, tonePosition: number, initialNoteKey: number, latestNoteErrors: NoteError[]
	): number {
		if (latestNoteErrors.length) {
			const toneError: NoteError = latestNoteErrors[Number(tonePosition)];
			return this.notesHelper.getNoteKey(toneError.noteName);
		} else {
			return initialNoteKey + this.notesHelper.calculateToneNumber(Number(tonePosition), instrument);
		}
	}
}
