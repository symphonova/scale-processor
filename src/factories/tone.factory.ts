import { ToneService } from '../services/tone.service';
import { DetailedDetectionRecord } from '../models/detection-record.model';
import { meanBy, sumBy, round } from 'lodash';
import { Tone, ToneError, TonePeakCorrectness, ToneTuningCorrectness, BinDelimitersConfiguration, ScalesConfig, NoteHelper } from 'symphonova-utils';

export class ToneFactory {
	private toneService: ToneService;
	private notesHelper: NoteHelper;

	constructor(private config: ScalesConfig) {
		this.toneService = new ToneService(config);
		this.notesHelper = new NoteHelper(config);
	}

	public fromDetailedDetectionRecords(noteKey: number,
	                                    tonePosition: number,
	                                    records: DetailedDetectionRecord[],
	                                    binDelimiters: BinDelimitersConfiguration): Tone {
		const totalDuration = this.toneService.calculateDetectionRecordsDuration(records);
		const isRecordInTune = this.toneService.areRecordsInValidBin(binDelimiters);
		const tunedRecords = records.filter(record => isRecordInTune(record.frequencyBin));
		const tunedDuration = sumBy(tunedRecords, record => record.duration);

		const tuningCorrectness: ToneTuningCorrectness = this.toneService.calculateTuningCorrectness(tunedRecords, totalDuration);
		const peakCorrectness: TonePeakCorrectness = this.toneService.calculatePeakCorrectness(tunedRecords);

		const peakMean: number = tunedRecords.length ? round(meanBy(tunedRecords, record => record.peak), 2) : null;
		const peakRMS: number = tunedRecords.length ? this.calculateRMSBy(tunedRecords, record => record.peak) : null;

		const toneErrors: ToneError[] = [
			...this.toneService.calculateDurationErrors(this.config.noteValidation, totalDuration, records.length),
			...this.toneService.calculateTuningErrors(this.config.noteValidation, tuningCorrectness),
			...this.toneService.calculatePeakErrors(this.config.noteValidation, peakCorrectness)
		];
		const score: number = this.toneService.calculateScore(tuningCorrectness, peakCorrectness);

		return {
			noteName: this.notesHelper.getName(noteKey),
			noteCentralFrequency: this.notesHelper.calculateFrequency(noteKey),
			tonePosition,
			totalDuration,
			tunedDuration,
			score,
			peakMean,
			peakRMS,
			tuningCorrectness,
			peakCorrectness,
			toneErrors,
			startTime: records[0] ? records[0].startTime : null,
			numberOfDetections: records.length
		};
	}

	private calculateRMSBy<T>(arr: T[], iteratee: (item: T) => number): number {
		const rms: number = Math.sqrt(arr.map((item: T) => {
			const val: number = iteratee(item);
			return val * val;
		}).reduce((acc, val) => acc + val, 0) / arr.length);
		return round(rms, 2);
	}
}
