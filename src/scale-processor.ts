import { catchError, map, mapTo, mergeMap, tap, toArray } from 'rxjs/operators';
import { combineLatest, EMPTY, from, Observable, of } from 'rxjs';
import { ScaleFactory } from './factories/scale.factory';
import { initializeArgv, Scale, log, MaxHelper, readFilesInDirectory, initDB, ScaleDBService, validateScalesConfig, ScalesConfig, EnvConfig, validateEnvConfig, ScaleMetadata, Tone, NoteError, ScaleErrorReportDBService, emptyDirectoryObs } from 'symphonova-utils';
import { DetectionRecord } from './models/detection-record.model';
import { ScalesService } from './services/scales.service';
import { DetectionRecordService } from './services/detection-record.service';

log.init('Scale Processor');

const argv = initializeArgv('script start --profileID 1968 --scalesDir /.../scales', {
	envConfigDict: { type: 'string', default: 'envConfig', describe: 'Name of the dictionary where the environment config is stored' },
	scalesConfigDict: { type: 'string', default: 'scalesConfig', describe: 'Name of the dictionary where the scales config is stored' },
	scalesDir: { type: 'string', demandOption: true, describe: 'Directory containing scale detections files (.txt)' },
	profileID: { type: 'string', describe: 'Profile ID to analyze. When not defined, system will work in standalone mode and will not communicate with the DB' },
});

log.info('Starting service...');

const processingStartTime: number = Date.now();

const envConfig$: Observable<EnvConfig> = MaxHelper.getDictionary(argv.envConfigDict).pipe(
	map(envConfigDict => validateEnvConfig(envConfigDict))
);

const scalesConfig$: Observable<ScalesConfig> = MaxHelper.getDictionary(argv.scalesConfigDict).pipe(
	map(envConfigDict => validateScalesConfig(envConfigDict))
);

let scaleDBService: ScaleDBService;
let scaleErrorReportDBService: ScaleErrorReportDBService;
let scaleFactory: ScaleFactory;
let scalesService: ScalesService;
let detectionRecordService: DetectionRecordService;

combineLatest([ envConfig$, scalesConfig$ ]).pipe(

	// initialization
	tap(([ envConfig, scalesConfig ]) => {
		initDB(envConfig.azure.dbConnectionString);
		scaleDBService = new ScaleDBService();
		scaleErrorReportDBService = new ScaleErrorReportDBService();
		scalesService = new ScalesService(scalesConfig);
		detectionRecordService = new DetectionRecordService(scalesConfig);
		scaleFactory = new ScaleFactory(scalesConfig);
	}),

	// analyze scales and log summary 
	mergeMap((): Observable<Scale> => {
		return readFilesInDirectory(argv.scalesDir as string).pipe(
			tap(file => log.info(`Processing file ${ file.name }`)),

			map((file): [ ScaleMetadata, DetectionRecord[] ] => {
				return [
					scalesService.parseFileNameToScaleMetadata(file.name),
					detectionRecordService.parseFileContent(file.content)
				];
			}),

			catchError(err => {
				log.error(`Skipping file. ${err}`);
				return EMPTY;
			}),

			mergeMap(([ scale, detections ]): Observable<Scale> => {
				const fileProcessingStartTime = Date.now();

				let latestNoteErrors$: Observable<NoteError[]> = of([]);
				if (argv.profileID) {
					log.info(`Retrieving latest note errors for scale`);
					latestNoteErrors$ = from(scaleErrorReportDBService.getLastestNoteErrors(argv.profileID, scale)).pipe(
						map(noteErrors => noteErrors || [])
					);
				}

				return latestNoteErrors$.pipe(
					map(latestNoteErrors => {
						const tones: Tone[] = scaleFactory.calculateTones(scale, detections, latestNoteErrors);
		
						const fileProcessingTime: number = Date.now() - fileProcessingStartTime;
						log.info(`Finished analysing scale for ${ scale.instrument } ${ scale.string } in ${ fileProcessingTime }ms`);
						const numberOfValidTones = tones.filter(tone => tone.toneErrors.length === 0).length;
						const numberOfInvalidTones = tones.length - numberOfValidTones;
						log.info(`Found ${ tones.length } notes, valid: ${ numberOfValidTones }, invalid: ${ numberOfInvalidTones }`);
		
						return { ...scale, tones };
					})
				)
			})
		);
	}),

	toArray(),
	tap(() => log.info(`Finished analysing all provided files. Total time: ${ Date.now() - processingStartTime }ms`)),

	// save scales in DB
	mergeMap(scales => {
		if (argv.profileID) {
			return from(scaleDBService.saveAll(scales).then(() => {
				log.info(`Scales saved in DB. Total time: ${ Date.now() - processingStartTime }ms`)
				return scales;
			}));
		} else {
			return of(scales);
		}
	}),

	// output scales to Max outlet
	mergeMap(scales => log.output({ data: scales })),
	tap(() => log.info('Scales sent to Max outlet')),

	// clean scalesDir
	mergeMap(scales => emptyDirectoryObs(argv.scalesDir).pipe(
		mapTo(scales)
	))

).subscribe(
	() => log.info(`Finished process. Total time: ${ Date.now() - processingStartTime }ms`),
	err => log.error(err.toString())
);