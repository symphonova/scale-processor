const path = require('path');
const nodeExternals = require('webpack-node-externals');

const { NODE_ENV = 'production' } = process.env;

module.exports = {
  entry: './src/scale-processor.ts',
  mode: NODE_ENV,
  target: 'node',
  watch: NODE_ENV === 'development',
  externals: [ nodeExternals() ],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'scale-processor.min.js'
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.(ts)$/,
        enforce: 'pre',
        exclude: /(node_modules|build|coverage)/,
        loader: 'eslint-loader',
        options: {
          // eslint options (if necessary)
        },
      },
      {
        test: /\.ts$/,
        use: [
          'ts-loader',
        ]
      }
    ],
  }
}
